# ftprotate

This is a Rust port of another repository of mine: [ftprotate (Python)](https://gitlab.com/julienjpk/ftprotate).

ftprotate is a simple tool to manage FTP backup rotation: give it a URL and a maximum file age, and it will recurse through the FTP server's file tree to delete every file that's too old.

## Usage

```text
$ ftprotate --help
USAGE:
    ftprotate [OPTIONS] <url> <age>

ARGS:
    <url>    the FTP directory URL to process
    <age>    the maximum file age, in seconds

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -a <anon_passwd>        the password to use for anonymous authentication
    -f <format>             a strptime-compatible format for timestamp parsing from filenames
    -n <netrc>              the path to the netrc file (default: ~/.netrc)
```

## Authentication

ftprotate offers no way to provide credentials on the command-line, since that would expose them. The typical way around this issue is to use environment variables, but in this case, there happens to be an even better way: [the netrc file](https://www.gnu.org/software/inetutils/manual/html_node/The-_002enetrc-file.html). Here's an example:

```text
machine ftp.example.com
login my_ftp_user
password my_ftp_password
```

Now, if you run ftprotate like so...

```text
ftprotate ftp://ftp.example.com 86400
```

... it will parse the hostname from the URL and look up the credentials in `~/.netrc`. You may specify another file path using the `-n` option. When no entry is found in the netrc file, ftprotate will login as the anonymous user. In that case, you may pick a custom anonymous password using the `-a` option.

## File age

By default, ftprotate will assume that the FTP server implements [RFC 3659](https://tools.ietf.org/html/rfc3659#) and provides a `MDTM` command to retrieve the modification timestamp of any file. In that case, this information is used to determine whether or not a file should be deleted.

If your FTP server does not implement `MDTM`, then it is possible to tell ftprotate how to parse a modification time from file names using the `-f` option. You can find a full list of available specifiers in [the documentation for the chrono crate](https://docs.rs/chrono/0.4.13/chrono/format/strftime/index.html).

## Partial rotation

You may specify a path in the URL in order to have ftprotate stay under a given subdirectory/

```text
ftprotate ftp://ftp.example.com/only/rotate/from/here 86400
```
