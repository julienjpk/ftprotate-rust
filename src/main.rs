/* ftprotate - a tool to delete old files from remote FTP servers
 * Copyright (C) Julien JPK <julienjpk@email.com>

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>. */

use clap::{Arg, App, ArgMatches};
use url::Url;
use netrc::{Netrc, Machine};
use ftp::FtpStream;
use chrono::offset::TimeZone;
use chrono::offset::utc::UTC;

/// Parse command-line arguments
fn parse_options() -> ArgMatches {
    App::new("ftprotate")
        .version("0.1.0")
        .author("Julien JPK <julienjpk@email.com>")
        .about("Rotates files on FTP servers")
        .arg(Arg::with_name("url")
             .about("the FTP directory URL to process")
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("age")
             .about("the maximum file age, in seconds")
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("netrc")
             .about("the path to the netrc file (default: ~/.netrc)")
             .short('n')
             .takes_value(true)
             .required(false))
        .arg(Arg::with_name("format")
             .about("a strptime-compatible format for timestamp parsing from \
                    filenames")
             .short('f')
             .takes_value(true)
             .required(false))
        .arg(Arg::with_name("anon_passwd")
             .about("the password to use for anonymous authentication")
             .short('a')
             .takes_value(true)
             .required(false))
        .get_matches()    
}

/// Locates the netrc file, parses it and returns its entries
///
/// * `custom_path` - an optional custom path to the netrc file
fn get_netrc(custom_path: Option<&str>) -> Result<Netrc, &str> {
    let mut home_netrc = dirs::home_dir().unwrap();
    home_netrc.push(".netrc");
    let home_netrc = home_netrc.as_path().to_str().unwrap();
    
    let netrc_path = custom_path.unwrap_or(home_netrc);
    let netrc_file = std::fs::File::open(netrc_path);
    if let Err(_e) = netrc_file {
        return Err("could not format the netrc file")
    };
    
    let netrc_file = std::io::BufReader::new(netrc_file.unwrap());
    match Netrc::parse(netrc_file) {
        Ok(n) => Ok(n),
        Err(_e) => Err("could not parse the netrc file")
    }
}

/// Rotates the current directory for the FTP stream
///
/// * `ftp` - the FTP stream
/// * `age_limit` - the file age limit in seconds
/// * `format` - an optional strftime-compatible string when MDTM is unavailable
fn rotate_directory(ftp: &mut FtpStream, age_limit: i64, format: Option<&str>) {
    let files = ftp.nlst(None).unwrap();
    for entry in files.iter() {
        match ftp.cwd(entry) {
            Ok(_n) => {
                rotate_directory(ftp, age_limit, format);
                ftp.cdup().unwrap();
            },
            Err(_e) => {
                let timestamp = match format {
                    Some(f) => match UTC.datetime_from_str(entry, f) {
                        Ok(dt) => dt,
                        Err(_ee) => { continue },
                    },
                    None => match ftp.mdtm(entry) {
                        Ok(dto) => match dto {
                            Some(dt) => dt,
                            None => { continue }
                        },
                        Err(_ee) => { continue }
                    }
                };

                let age = UTC::now() - timestamp;
                let age = age.num_seconds();
                if age > age_limit {
                    if let Err(e) = ftp.rm(entry) {
                        eprintln!("Could not delete {}/{}: {}",
                                  ftp.pwd().unwrap_or("?".to_string()),
                                  entry, e);
                    }
                }
            }
        }
    }
}

fn main() {
    let options = parse_options();

    let url = options.value_of("url").unwrap();
    let url = Url::parse(url).unwrap_or_else(|e| {
        eprintln!("The provided URL \"{}\" is invalid: {}", url, e);
        std::process::exit(1);
    });

    let age_limit = options.value_of("age").unwrap();
    let age_limit = age_limit.parse::<i64>().unwrap_or_else(|e| {
        eprintln!("The provided age value \"{}\" is invalid: {}", age_limit, e);
        std::process::exit(1);
    });

    let format = options.value_of("format");
    let netrc = match get_netrc(options.value_of("netrc")) {
        Ok(n) => n,
        Err(e) => {
            eprintln!("{}", e);
            std::process::exit(1);
        }
    };

    let hostname = url.host_str().unwrap();
    let netrc_lookup = match url.port() {
        Some(p) => format!("{}:{}", hostname, p),
        None => hostname.to_string()
    };
    let port = url.port().unwrap_or(21);

    let default_machine = Machine {
        login: String::from("anonymous"),
        password: match options.value_of("anon_passwd") {
            Some(s) => Some(String::from(s)),
            None => Some(String::from("guest"))
        },
        account: None,
        port: Some(port)
    };
    
    let machine = match netrc.hosts.iter().find(|h| { h.0 == netrc_lookup }) {
        Some(m) => &m.1,
        None => &default_machine
    };

    let host_port = format!("{}:{}", hostname, port);
    let mut ftp_stream = FtpStream::connect(&host_port).unwrap_or_else(|e| {
        eprintln!("Could not connect to {}: {}", &host_port, e);
        std::process::exit(1);        
    });

    let default_password = String::from("");
    let password = machine.password.as_ref().unwrap_or(&default_password);
    ftp_stream.login(&machine.login, &password).unwrap_or_else(|e| {
        eprintln!("Could not login to {}: {}", &host_port, e);
        std::process::exit(1);        
    });

    let path = url.path();
    ftp_stream.cwd(path).unwrap_or_else(|e| {
        eprintln!("Could not move to {} : {}", path, e);
        std::process::exit(1);        
    });

    rotate_directory(&mut ftp_stream, age_limit, format);
    ftp_stream.quit().unwrap();
}
